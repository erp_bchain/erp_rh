from django import forms
from .models import *
from procesos.models import ProyectoInfo

class AcademicInfoForm(forms.ModelForm):
    class Meta:
        model = AcademicInfo
        exclude = ['numero_registro', 'estado', 'personal_info', 'direccion_info', 'defensa_info', 'contacto_emergencia_info', 'informacion_academica_previa_info', 'certificado_academico_previo', 'is_delete']
        widgets = {
            'informacion_proyecto': forms.Select(attrs={'class': 'form-control'})
        }

class PersonalInfoForm(forms.ModelForm):
    class Meta:
        model = PersonalInfo
        fields = '__all__'
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'foto': forms.ClearableFileInput(attrs={'class': 'form-control'}),
            'grupo_sanguineo': forms.Select(attrs={'class': 'form-control'}),
            'fecha_de_nacimiento': forms.TextInput(attrs={'class': 'form-control'}),
            'genero': forms.Select(attrs={'class': 'form-control'}),
            'numero_telefono': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
            'certificado_de_nacimiento_no': forms.TextInput(attrs={'class': 'form-control'}),
            'religion': forms.Select(attrs={'class': 'form-control'}),
            'nacionalidad': forms.Select(attrs={'class': 'form-control'})
        }

class DireccionConsultorInfoForm(forms.ModelForm):
    class Meta:
        model = DireccionConsultorInfo
        fields = '__all__'
        widgets = {
            'zona': forms.Select(attrs={'class': 'form-control'}),
            'locacion': forms.Select(attrs={'class': 'form-control'}),
            'anexo': forms.Select(attrs={'class': 'form-control'}),
            'cp': forms.TextInput(attrs={'class': 'form-control'})
        }

        def __init__(self, *args, **kwargs):
            super().__init__(*args, **kwargs)
            self.fields['locacion'].queryset = Locacion.objects.none()

            if 'locacion' in self.data:
                try:
                    zona_id = int(self.data.get('zona'))
                    self.fields['locacion'].queryset = Locacion.objects.filter(zona_id=zona_id).order_by('nombre')
                except (ValueError, TypeError):
                    pass
            elif self.instance.pk:
                self.fields['locacion'].queryset = self.instance.zona.locacion_set.order_by('nombre')

            self.fields['anexo'].queryset = Anexo.objects.none()

            if 'anexo' in self.data:
                try:
                    locacion_id = int(self.data.get('locacion'))
                    self.fields['anexo'].queryset = Union.objects.filter(locacion_id=locacion_id).order_by('name')
                except (ValueError, TypeError):
                    pass
            elif self.instance.pk:
                self.fields['anexo'].queryset = self.instance.locacion.anexo_set.order_by('nombre')


class DefensaInfoForm(forms.ModelForm):
    class Meta:
        model = DefensaInfo
        fields = '__all__'
        widgets = {
            'nombre_padre': forms.TextInput(attrs={'class': 'form-control'}),
            'numero_telefono_padre': forms.TextInput(attrs={'class': 'form-control'}),
            'ocupacion_padre': forms.Select(attrs={'class': 'form-control'}),
            'ingresos_anuales_del_padre': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre_madre': forms.TextInput(attrs={'class': 'form-control'}),
            'numero_telefono_madre': forms.TextInput(attrs={'class': 'form-control'}),
            'ocupacion_madre': forms.Select(attrs={'class': 'form-control'}),
            'nombre_supervisor': forms.TextInput(attrs={'class': 'form-control'}),
            'numero_telefono_supervisor': forms.TextInput(attrs={'class': 'form-control'}),
            'email_supervisor': forms.TextInput(attrs={'class': 'form-control'}),
            'relacion_con_consultor': forms.Select(attrs={'class': 'form-control'}),
        }

class DatosdeContactodeEmergenciaForm(forms.ModelForm):
    class Meta:
        model = DatosdeContactodeEmergencia
        fields = '__all__'
        widgets = {
            'nombre_del_defensa_de_emergencia': forms.TextInput(attrs={'class': 'form-control'}),
            'direccion': forms.Textarea(attrs={'class': 'form-control'}),
            'relacion_con_consultor': forms.Select(attrs={'class': 'form-control'}),
            'numero_telefono': forms.TextInput(attrs={'class': 'form-control'}),
            'email': forms.TextInput(attrs={'class': 'form-control'}),
        }

class AcademicadePreviaInfoForm(forms.ModelForm):
    class Meta:
        model = AcademicadePreviaInfo
        fields = '__all__'
        widgets = {
            'nombre_instituto': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre_evaluacion': forms.TextInput(attrs={'class': 'form-control'}),
            'grupo': forms.TextInput(attrs={'class': 'form-control'}),
            'gpa': forms.TextInput(attrs={'class': 'form-control'}),
            'tabla_roles': forms.TextInput(attrs={'class': 'form-control'}),
            'anio_actual': forms.TextInput(attrs={'class': 'form-control'}),
        }

class CertificadoAcademicoPrevioForm(forms.ModelForm):
    class Meta:
        model = CertificadoAcademicoPrevio
        fields = '__all__'

class BusquedaConsultoresForm(forms.Form):
    informacion_proyecto = forms.ModelChoiceField(required=False, queryset=ProyectoInfo.objects.all())
    numero_registro = forms.IntegerField(required=False, widget=forms.NumberInput(attrs={'placeholder': 'Registration No', 'aria-controls': 'DataTables_Table_0'}))

class ConsultorInscritoForm(forms.Form):
    nombre_proyecto = forms.ModelChoiceField(queryset=ProyectoInfo.objects.all())

class Inscripcion_de_ConsultorForm(forms.Form):
    nombre_proyecto = forms.ModelChoiceField(queryset=RegistrodeProyecto.objects.all(), widget=forms.Select(attrs={'class': 'form-control'}))
    roll_no = forms.IntegerField(widget=forms.NumberInput(attrs={'placeholder': 'Ingrese Roll', 'class': 'form-control'}))

class BuscarConsultorInscritoForm(forms.Form):
    reg_proyecto = forms.ModelChoiceField(queryset=RegistrodeProyecto.objects.all())
    roll_no = forms.IntegerField(required=False, widget=forms.NumberInput(attrs={'placeholder': 'Ingrese Roll'}))