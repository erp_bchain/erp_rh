from django.shortcuts import render
from django.contrib.auth.models import User
from .models import UserProfile
from .forms import ProfileForm
# Create your views here.

def profile(request):
    perfil = UserProfile.objects.get(id=request.user.id)
    context = {
        'perfil': perfil
    }
    return render(request, 'cuenta/profile.html', context)

def actualizar_perfil(request):
    perfil = UserProfile.objects.get(id=request.user.id)
    forms = ProfileForm(instance=perfil)
    if request.method == 'POST':
        forms = ProfileForm(request.POST, request.FILES, instance=perfil)
        if forms.is_valid():
            forms.save()
    context = {
        'forms': forms
    }
    return render(request, 'cuenta/update-profile.html', context)
