from django.urls import path
from .views import profile, actualizar_perfil

urlpatterns = [
    path('profile/', profile, name='profile'),
    path('update/', actualizar_perfil, name='update-profile')
]