from django.shortcuts import render
from django.contrib.auth.decorators import login_required
import consultor
import gerente
import empleado
import procesos

@login_required(login_url='login')
def home_page(request):
    total_consultor = consultor.models.AcademicInfo.objects.count()
    total_gerente = gerente.models.PersonalInfo.objects.count()
    total_empleado = empleado.models.PersonalInfo.objects.count()
    total_class = procesos.models.RegistrodeProyecto.objects.count()
    context = {
        'consultor': total_consultor,
        'gerente': total_gerente,
        'empleado': total_empleado,
        'total_class': total_class,
    }
    return render(request, 'home.html', context)
