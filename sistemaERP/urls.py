"""sistemaERP URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from .views import home_page
from . import settings
from django.contrib.staticfiles.urls import static, staticfiles_urlpatterns

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_page, name='home'),
    path('', include('administracion.urls')),
    path('gerente/', include('gerente.urls')),
    path('consultor/', include('consultor.urls')),
    path('procesos/', include('procesos.urls')),
    path('empleado/', include('empleado.urls')),
    path('resultado/', include('resultado.urls')),
    path('domicilio/', include('domicilio.urls')),
    path('cuenta/', include('cuenta.urls')),
    path('asistencia/', include('asistencia.urls')),
    # path('advanced_filters/', include('advanced_filters.urls'))
]
urlpatterns += staticfiles_urlpatterns()
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
