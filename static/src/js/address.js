$("#id_zona").change(function () {
  var url = $("#StudentRegistrationForm").attr("data-locacion-url");
  var zonaId = $(this).val();

  $.ajax({
    url: url,
    data: {
      'zona': zonaId
    },
    success: function (data) {
      $("#id_locacion").html(data);
    }
  });

});
$("#id_locacion").change(function () {
  var url = $("#StudentRegistrationForm").attr("data-locacion-url");
  var locacionId = $(this).val();

  $.ajax({
    url: url,
    data: {
      'locacion': locacionId
    },
    success: function (data) {
      $("#id_anexo").html(data);
    }
  });

});
