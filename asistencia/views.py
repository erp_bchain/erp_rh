from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status

from .forms import BuscarConsultorRegistradoFormularioForm
from consultor.models import ConsultorInscrito
from consultor.models import RegistrodeProyecto
from .models import AsistenciaConsultores 

# Create your views here.
def asistencia_consultor(request):
    forms = BuscarConsultorRegistradoFormularioForm()
    nombre_proyecto = request.GET.get('reg_proyecto', None)
    if nombre_proyecto:
        proyecto_info = RegistrodeProyecto.objects.get(id=nombre_proyecto)
        consultor = ConsultorInscrito.objects.filter(nombre_proyecto=nombre_proyecto)
        context = {
            'forms': forms,
            'consultor': consultor,
            'proyecto_info': proyecto_info
        }
        return render(request, 'asistencia/consultor-attendance.html', context)
    context = {
        'forms': forms,
    }
    return render(request, 'asistencia/consultor-attendance.html', context)

class SetAsistencia(APIView):
    def get(self, request, std_class, std_roll):
        try:
            AsistenciaConsultores.objects.Crearasistencia(std_class, std_roll)
            return Response({'estado': 'Success'}, estado=estado.HTTP_200_OK)
        except:
            return Response({'estado': 'Failed'}, estado=estado.HTTP_400_BAD_REQUEST)