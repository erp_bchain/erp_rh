# Generated by Django 3.1.14 on 2022-10-18 07:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('asistencia', '0002_asistenciaconsultores_consultor'),
        ('procesos', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='asistenciaconsultores',
            name='nombre_proyecto',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='procesos.registrodeproyecto'),
        ),
        migrations.AlterUniqueTogether(
            name='asistenciaconsultores',
            unique_together={('consultor', 'date')},
        ),
    ]
