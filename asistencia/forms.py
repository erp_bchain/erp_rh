from django import forms
from procesos.models import RegistrodeProyecto

class BuscarConsultorRegistradoFormularioForm(forms.Form):
    reg_proyecto =forms.ModelChoiceField(queryset=RegistrodeProyecto.objects.all())
