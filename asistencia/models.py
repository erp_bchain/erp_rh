from django.db import models
from procesos.models import RegistrodeProyecto
from consultor.models import ConsultorInscrito
# Create your models here.

class ManagerAsistencia(models.Manager):
    def Crearasistencia(self, std_class, std_roll):
        std_cls = RegistrodeProyecto.objects.get(nombre=std_class)
        std = ConsultorInscrito.objects.get(roll=std_roll, nombre_proyecto=std_cls)
        std_att = AsistenciaConsultores.objects.create(
            nombre_proyecto=std_cls,
            consultor = std,
            estado = 1
        )
        return std_att
class AsistenciaConsultores(models.Model):
    nombre_proyecto = models.ForeignKey(RegistrodeProyecto, on_delete=models.CASCADE)
    consultor = models.ForeignKey(ConsultorInscrito, on_delete=models.CASCADE)
    estado = models.IntegerField(default=0)
    date = models.DateField(auto_now_add=True)

    objects = ManagerAsistencia()

    class Meta:
        unique_together = ['consultor', 'date']

    def __str__(self):
        return str(self.consultor.consultor.personal_info.nombre)
