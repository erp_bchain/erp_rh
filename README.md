# ERP_RH
ERP con registro y acceso de usuarios
Instalar las siguiente librerias:

asgiref==3.5.0
certifi==2019.6.16
chardet==3.0.4
Django==3.1.14
django-jazzmin==2.4.9
djangorestframework==3.11.2
idna==2.8

python manage.py makemigration
python manage.py runserver