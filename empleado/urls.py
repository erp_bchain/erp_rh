from django.urls import path

from . import views

urlpatterns = [
    path('registration', views.registro_gerente, name='empleado-registration'),
    path('list', views.lista_gerente, name='empleado-list'),
    path('cargar_locacion', views.cargar_locacion, name='cargar_locacion'),
]
