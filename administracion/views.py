from django.shortcuts import render,redirect
from django.contrib.auth import authenticate,login,logout
from django.contrib.auth.models import User
from gerente.models import Departmento,Designacion

from .forms import *
# Create your views here.
def admin_login(request):
    forms = AdminLoginForm()
    if request.method == 'POST':
        forms = AdminLoginForm(request.POST)
        if forms.is_valid():
            username = forms.cleaned_data['username']
            password = forms.cleaned_data['password']
            user = authenticate(username=username, password=password)
            if user:
                login(request, user)
                return redirect('home')
    context = {'forms': forms}
    return render(request, 'administracion/login.html', context)

def admin_logout(request):
    logout(request)
    return redirect('login')

def add_designation(request):
    forms = AddDesignationForm()
    if request.method == 'POST':
        forms = AddDesignationForm(request.POST)
        if forms.is_valid():
            forms.save()
            return redirect('designacion')
    designacion = Designacion.objects.all()
    context = {'forms': forms, 'designacion': designacion}
    return render(request, 'administracion/designacion.html', context)