from django.db import models

# Create your models here.
class Designacion(models.Model):
    nombre = models.CharField('Nombre',max_length=100, unique=True)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.nombre