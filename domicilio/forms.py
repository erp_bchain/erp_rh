from django import forms
from .models import Zona,Locacion,Anexo


class ZonaForm(forms.ModelForm):
    class Meta:
        model = Zona
        fields = '__all__'
        widgets = {
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
        }

class LocacionForm(forms.ModelForm):
    class Meta:
        model = Locacion
        fields = '__all__'
        widgets = {
            'zona': forms.Select(attrs={'class': 'form-control'}),
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
        }

class AnexoForm(forms.ModelForm):
    class Meta:
        model = Anexo
        fields = '__all__'
        widgets = {
            'zona': forms.Select(attrs={'class': 'form-control'}),
            'locacion': forms.Select(attrs={'class': 'form-control'}),
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
        }
