from django.db import models

# Create your models here.
class Zona(models.Model):
    nombre = models.CharField('Zona',max_length=45, unique=True)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.nombre

class Locacion(models.Model):
    zona = models.ForeignKey(Zona, on_delete=models.CASCADE)
    nombre = models.CharField('Localidad',max_length=45, unique=True)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.nombre

class Anexo(models.Model):
    zona = models.ForeignKey(Zona, on_delete=models.CASCADE)
    locacion = models.ForeignKey(Locacion, on_delete=models.CASCADE)
    nombre = models.CharField('Anexo-Región',max_length=45, unique=True)
    date = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.nombre
