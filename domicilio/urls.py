from django.urls import path
from . import views


urlpatterns = [
    path('zona', views.add_zona, name='zona'),
    path('locacion', views.add_locacion, name='locacion'),
    path('anexo', views.add_anexo, name='anexo'),
]
