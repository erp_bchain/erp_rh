# Generated by Django 3.1.14 on 2022-10-18 07:19

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Zona',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=45, unique=True, verbose_name='Zona')),
                ('date', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Locacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=45, unique=True, verbose_name='Localidad')),
                ('date', models.DateField(auto_now_add=True)),
                ('zona', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='domicilio.zona')),
            ],
        ),
        migrations.CreateModel(
            name='Anexo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=45, unique=True, verbose_name='Anexo-Región')),
                ('date', models.DateField(auto_now_add=True)),
                ('locacion', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='domicilio.locacion')),
                ('zona', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='domicilio.zona')),
            ],
        ),
    ]
